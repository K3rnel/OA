
#define B 843
#define R 20
float rpm[2];
double velocity1,velocity2;
class Motor {
    unsigned int br_pin, dir_pin, pwm_pin, en_pin;
    boolean cclk;
  public:
    int reset;
   
    Motor(unsigned int pin1, unsigned int pin2, unsigned int pin3, boolean stat1) {
      reset = 0;
      //br_pin = pin0;
      pwm_pin = pin1;
      en_pin = pin2;
      dir_pin = pin3;
      cclk = stat1;

      //pinMode(br_pin, OUTPUT);
      pinMode(dir_pin, OUTPUT);
      pinMode(pwm_pin, OUTPUT);
      pinMode(en_pin, OUTPUT);

      //digitalWrite(br_pin, HIGH);
      digitalWrite(en_pin, HIGH);
      digitalWrite(dir_pin, HIGH);
      analogWrite(pwm_pin, 25 );
    }

    void enable(boolean en) {
      digitalWrite(en_pin, en);
    }

      void motor_move(int vel) {
      //brake(LOW);
      int val;
      if (vel > 0)
        digitalWrite(dir_pin, cclk);
      else if (vel < 0)
        digitalWrite(dir_pin, !cclk);
      val = constrain(abs(vel), 0, 107.2);
      val = map(val, 0, 107.2, 25, 229);
      analogWrite(pwm_pin, val);
    }

};

Motor motors[2] = {Motor(9, 23, 25, 1), Motor(8, 22, 24, 0)}; //motors 1, 2

//motors[4] = { Motor(8, 23, 25, 0), Motor(9, 22, 24, 1)}; //motors 1, 2
/*-----------------------
 motor 1 - 8, 23, 25, 8 - pwm, 23 - enable, 25 - dir
 motor 2 - 9, 22, 26, 9 - pwm, 22 - enable, 24 - dir

 -------------------------*/
void bot_motion(float v, float w)
{ 
  //assuming v in input is cm/sec
  rpm[0]=(v+w*B)*60/(2*PI)/R;
  rpm[1]=(v-w*B)*60/(2*PI)/R;

  for(int i=0;i<2;i++) 
    motors[i].motor_move(rpm[i]);

}

//void bot_brake(int str)
//{
//  for(int i=0;i<4;i++)
  //    motors[i].brake(HIGH, str);   
//}

  
void setup()
{
  Serial.begin(9600);
analogReadResolution(12);
  motors[0].enable(HIGH);
  motors[1].enable(HIGH);
}
void loop()
{
  //   bot_brake(320);
//  bot_motion(20,0); //velocity in cm/sec && angular velocity in radians/sec;
  //motors[0].motor_move(10);
  motors[1].motor_move(10);
}

