# TEAM ABHIYAAN 
## Project: K3rnel
In this project we are aiming to develop an **Autnomous Ground Vehicle** which will represent IIT Madras in **IGVC 2017**

### Getting Started
#### Softwares
* Ubuntu 14.04
* ROS Indigo
* Arduino
* Terminator (Optional but recommended)
* Sublime Text (Optional)
* Husky Simulator: `sudo apt-get install ros-indigo-husky*` works properly in bash

#### Resolving Dependencies and permissions
* After you are done with pulling/cloning the repository, you need to install dependencies. Make sure that rosdep is initialised, after that
    `cd ~ && rosdep install --from-paths OA --ignore-src --rosdistro=indigo`

* Add user name to dailout group: `sudo usermod -a -G dialout USER` with `USER` being your username

* Use `chmod g+x,g+w,g+r -R <<file/folder>>` for giving permissions to the group`g` for executing`+x`, writing`+w` and reading`+r` recursively `-R` over all the folders specified

#### Commands
There are different launch files depending on the purpose. You can find them in `abhiyan_nav/launch`
Launch files are organised into 3 levels for modularity. 

##### Level 1: Basic launchers 
These launch files are found in `abhiyan_nav/launch/Basic`. This launch files include launching sensors, bagfiles and transforms.

* **Sensors** : Launches all the sensors

* **Transforms** : Launches all the relevant transforms along with ekf

* **Bagfile** : Launches the bagfiles for simulation of algorithms.

##### Level 2: Mapping Algorithms
These launch files are found in `abhiyan_nav/launch/Core`. These launch files are used primarily for `map -> odom` transform. 

* **gmapping** : Launches gmapping along with specified parameters

* **amcl** : Launches AMCL along with specified parameters

* **move_base** : Launches move_base along with costmap and TEB planner 

* **navsat_ekf** : Launches ekf localisation, mapping and navsat transform node

##### Level 3: Top level
These launch files are found in `abhiyan_nav/launch/Mappers`. They comprise of launching previous levels with various combinations.

* **amcl_demo** : Launches AMCL with along one of the map present in `abhiyan_nav/maps`. You can send the map name to the launch file in the command line. For example, currently *CFI28-1-17* map is present. You can tell amcl to launch this map at the command line by 

     `roslaunch abhiyan_nav amcl_demo.launch map_file:=CFI28-1-17.yaml`

* **gmapping_demo** : Launches gmapping along with move_base etc

* **hector_mapping** : Launches hector slam along with move_base etc

* **move_base_mapless_demo** : Launches move base without static map

* **navsat_mapping** : Launches navsat transform node and *map->odom* using ekf and launches move_base_mapless_demo for costmap

Additional functionalities like teleop is also included in them. 
For launching teleop along with anyone of the above launch files, additional argument can be provided in the commandline. For example launching teleop along with gmapping, 

`roslaunch abhiyan_nav gmapping_demo.launch teleop:=true`

This will launch teleop instance in a new terminal

**For Simulation:**
For world with obstacles

`roslaunch husky_gazebo husky_playpen.launch`

For world without obstacles

`roslaunch husky_gazebo husky_empty_world.launch`

This will launch the simulator setting up virtual sensors and their Transformations. Checkout ros robots husky website for more information on their launch files. Very useful for simulation of gmapping, 

`roslaunch husky_navigation gmapping_demo.launch`

**Useful Commands**
* piping out throug `grep` for required string matching
* `rosbag filter <input> <output> "topic==<<topic1>> or topic==<<topic2>> "`

#### TO DOs:
1. Update the Softwares list
2. Remotely accessing the nodes using ROS multiple masters
3. Updating `abhiyan_nav` with gazebo worlds description
4. Updating `abhiyan_nav/Basic` with Gazebo, sensor plugins and urdf
5. Updating the **Transforms** launch file with Joint state publisher and robot state publisher
6. Need to argumentaise the USB port for Arduino, IMU, GPS and Camera for launch files. Will it be usefull? How to implement it?
6. Research about docker and update the same in readme file.

refer this [link](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md) for markdown styles available in gitlab 
