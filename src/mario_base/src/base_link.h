// Configuration of Robot base

#ifndef BASE_LINK_H
#define BASE_LINK_H

const float CONST_PI              = 3.1415;
const float WHEEL_TRACK           = 0.615;  // distance between wheels in m
const float WHEEL_RADIUS          = 0.200;  // radius of wheels
const uint32_t TICKS_PER_ROTATION = 21504/2;

#endif
