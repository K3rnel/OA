/*
  Command definitions for base link
*/

#define REPLY_SUCCESS 0xA5
#define REPLY_NOACT   0xA6  // No action required
#define REPLY_ERROR   0xA8

#define COMMAND_START 0x55
#define REPLY_START   REPLY_SUCCESS

#define COMMAND_STOP  0x56
#define REPLY_STOP    REPLY_SUCCESS

#define COMMAND_RESET 0x57
#define REPLY_RESET   REPLY_SUCCESS

#define COMMAND_PING  0x58
#define REPLY_PING    0xA8

#define COMMAND_SEND_ENC_DATA 0x70  // command to send encoder ticks

#define COMMAND_SET_MOTOR 0x65
#define COMMAND_DIR_FWD   0x01
#define COMMAND_DIR_REV   0x02

typedef struct {
  int32_t i32_ticksLeft;
  int32_t i32_ticksRight;
}__attribute__((__packed__))encoder_data_t;

typedef struct {
  uint8_t u8_motorDirection;  // upper 4 bits for Left Motor, lower nibble for right motor
  uint8_t u8_pwmLeft;
  uint8_t u8_pwmRight;
}__attribute__((__packed__))motor_command_t;
