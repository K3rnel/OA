// Program for interfacing with low level controller in base_link
// Author: Vishnu Raj
#include <unistd.h>
#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Twist.h>
#include <tf/transform_broadcaster.h>

#include <mario_base/EncoderData.h>
#include "serial_port.hpp"
#include "commands.h"
#include "base_link.h"

// odometry related information
float odom_pose_x   = 0.00;
float odom_pose_y   = 0.00;
float odom_orie_yaw = 0.00;
encoder_data_t ticks_curr = { 0, 0 };
encoder_data_t ticks_prev = { 0 ,0 };
ros::Time enc_stamp_prev;

int8_t initBase( void );    // function to initialize robot base

void cb_cmdVel( const geometry_msgs::Twist::ConstPtr& msg_cmdVel ); // callback for cmdVel message

SerialPort Serial( "/dev/ttyACM0", 115200 );

int main( int argc, char **argv ) {
  // start ROS node
  ros::init( argc, argv, "base_bridge" );
  ros::NodeHandle hNode( "~" );

  // try to initialize robot base
  if( !initBase() ) {
    std::cout << "Failed to initialize robot base. Exiting..!" << std::endl;
    return 1;
  }


  // register publisher for encoder ticks
  ros::Publisher pub_encTicks = hNode.advertise<mario_base::EncoderData>( "ticks_raw", 16 );
  // register publisher for odom
  ros::Publisher pub_odometry = hNode.advertise<nav_msgs::Odometry>( "odom", 16 );
  // register TF broadcaster
  // tf::TransformBroadcaster tfbc_odom;

  // subscribe to cmd_vel
  ros::Subscriber sub_cmdVel  = hNode.subscribe( "cmd_vel", 16, cb_cmdVel );

  mario_base::EncoderData msg_ticks_curr;

  ros::Rate loop_rate( 10 );  // 10 times per second

  Serial.write( COMMAND_SEND_ENC_DATA );  // send first read command
  while( ros::ok() ) {
    if( Serial.available() >= sizeof(ticks_curr) ) {
      Serial.read( (uint8_t*) &ticks_curr, sizeof(ticks_curr) );

      ros::Time this_msg_stamp  = ros::Time::now();
      // ----------------- Publish ticks data ---------------------------------
      // fill in timestaps
      msg_ticks_curr.header.stamp    = this_msg_stamp;
      msg_ticks_curr.header.frame_id = "base_link";
      // fill in data
      msg_ticks_curr.ticks_left  = ticks_curr.i32_ticksLeft;
      msg_ticks_curr.ticks_right = ticks_curr.i32_ticksRight;
      // publish
      pub_encTicks.publish( msg_ticks_curr );

      // ---------------- Publish odom message ---------------------------------
      nav_msgs::Odometry msg_odom;

      int delta_ticks_left  = ticks_curr.i32_ticksLeft - ticks_prev.i32_ticksLeft;
      int delta_ticks_right = ticks_curr.i32_ticksRight - ticks_prev.i32_ticksRight;
      ros::Duration update_interval = msg_ticks_curr.header.stamp - enc_stamp_prev;
      float delta_time = update_interval.sec + update_interval.nsec*1e-9;

      // calculate twist data
      // float velocity_left   = 2 * CONST_PI * WHEEL_RADIUS / delta_time
      //                               * float(delta_ticks_left)/TICKS_PER_ROTATION;
      // float velocity_right  = 2 * CONST_PI * WHEEL_RADIUS / delta_time
      //                               * float(delta_ticks_right)/TICKS_PER_ROTATION;
      float velocity_left   = 0.91*3 / delta_time
                                * float(delta_ticks_left)/25287;
      float velocity_right  = 0.91*3/ delta_time
                                * float(delta_ticks_right)/24924;
      float velocity_base   = 0.5 * ( velocity_left + velocity_right ); // final velocity of robot base link

      ROS_DEBUG_STREAM( "delta_ticks = " << delta_ticks_left << ", " << delta_ticks_right
                        << "    " << delta_time << "sec" << std::endl
                        << "    v = " << velocity_left << ", " << velocity_right << "m/s" );

      float velocity_angular  = (velocity_right-velocity_left) / WHEEL_TRACK;
      float delta_theta       = velocity_angular * delta_time;

      // fill in twist data
      msg_odom.twist.twist.linear.z = 0.00f;  // no wings!!
      if( abs(velocity_right-velocity_left) > 1e-4 ) { // if we have considerable difference in velocities
        msg_odom.twist.twist.linear.x = velocity_base * cos(delta_theta);
        msg_odom.twist.twist.linear.y = velocity_base * sin(delta_theta);
      } else {
        msg_odom.twist.twist.linear.x = velocity_base;
        msg_odom.twist.twist.linear.y = 0.00f;
      }
      msg_odom.twist.twist.angular.x  = 0.00f;
      msg_odom.twist.twist.angular.y  = 0.00f;
      msg_odom.twist.twist.angular.z  = velocity_angular;
      // covariance representation order: ( ν_x, ν_y, ν_z, ω_x, ω_y, ω_z )
      msg_odom.twist.covariance = { // @TODO: Fix this matrix based on data
        1.00, 1e-4, 1e-4, 1e-4, 1e-4, 1e-4,
        1e-4, 1.00, 1e-4, 1e-4, 1e-4, 1e-4,
        1e-4, 1e-4, 1e+6, 1e-4, 1e-4, 1e-4,
        1e-4, 1e-4, 1e-4, 1e+6, 1e-4, 1e-4,
        1e-4, 1e-4, 1e-4, 1e-4, 1e+6, 1e-4,
        1e-4, 1e-4, 1e-4, 1e-4, 1e-4, 1.00
      };

      // calculate pose data
      float delta_x = velocity_base * cos(delta_theta) * delta_time;
      float delta_y = velocity_base * sin(delta_theta) * delta_time;
      odom_pose_x += (delta_x*cos(odom_orie_yaw)-delta_y*sin(odom_orie_yaw));
      odom_pose_y += (delta_x*sin(odom_orie_yaw)+delta_y*cos(odom_orie_yaw));
      odom_orie_yaw += delta_theta;

      // fill in pose data
      msg_odom.pose.pose.position.x   = odom_pose_x;
      msg_odom.pose.pose.position.y   = odom_pose_y;
      msg_odom.pose.pose.position.z   = 0.00f;
      msg_odom.pose.pose.orientation  = tf::createQuaternionMsgFromYaw( odom_orie_yaw );
      msg_odom.pose.covariance = {  // covariance matrix for pos, same structure as twist covariance
        1e+6, 1e-4, 1e-4, 1e-4, 1e-4, 1e-4,
        1e-4, 1e+6, 1e-4, 1e-4, 1e-4, 1e-4,
        1e-4, 1e-4, 1e+6, 1e-4, 1e-4, 1e-4,
        1e-4, 1e-4, 1e-4, 1e+6, 1e-4, 1e-4,
        1e-4, 1e-4, 1e-4, 1e-4, 1e+6, 1e-4,
        1e-4, 1e-4, 1e-4, 1e-4, 1e-4, 1e+6,
      };

      // fill in header
      msg_odom.header.stamp     = this_msg_stamp;
      msg_odom.header.frame_id  = "odom";
      msg_odom.child_frame_id   = "base_link";

      // ... and shoot!!
      pub_odometry.publish( msg_odom );

      // ---------------- Publish transform ------------------------------------
      // geometry_msgs::TransformStamped tf_odom;
      // tf_odom.header.stamp    = this_msg_stamp;
      // tf_odom.header.frame_id = "odom";
      // tf_odom.child_frame_id  = "base_link";

      // tf_odom.transform.translation.x = odom_pose_x;
      // tf_odom.transform.translation.y = odom_pose_y;
      // tf_odom.transform.translation.z = 0.00f;
      // tf_odom.transform.rotation      = msg_odom.pose.pose.orientation;

      // tfbc_odom.sendTransform( tf_odom );

      // save this data for next loop
      ticks_prev      = ticks_curr;
      enc_stamp_prev  = msg_ticks_curr.header.stamp;

      // send next request
      Serial.write( COMMAND_SEND_ENC_DATA );
    }
    ros::spinOnce();
    loop_rate.sleep();
  }

  return 1;
}

/*
  Function to initialize robot base
*/
int8_t initBase( void ) {
  ROS_INFO( "Connecting to base link" );
  if( !Serial ) {
      ROS_INFO( "Failed to connect to base link" );
      return 0;
  }
  sleep( 2 );   // 2 seconds for baselink controller to boot.
  Serial.flush();
  ROS_INFO( "Connected to base link" );

  uint8_t reply = 0x00; //

  // // sending stop command - due is not reseting on new connection
  // std::cout << "Sending STOP  command ... ";  std::cout.flush();
  // Serial.write( COMMAND_STOP );
  // reply = Serial.read();
  // std::cout << "RESPONSE : " << std::hex << std::uppercase << int(reply) << std::endl;

  // do soft reset
  ROS_INFO( "Sending RESET command" );
  Serial.write( COMMAND_RESET );
  reply = Serial.read();
  if( reply == uint8_t(REPLY_RESET) ) {
    ROS_INFO_STREAM( "Reset Success with code " << " 0x" << std::hex << std::uppercase << int(reply)  );
  } else {
    ROS_INFO_STREAM( "Reset Failed with code" << " 0x" << std::hex << std::uppercase << int(reply) );
  }

  // bootup commands
  ROS_INFO_STREAM( "Sending START command" );
  Serial.write( COMMAND_START );
  reply = Serial.read();
  if( reply == uint8_t(REPLY_START) || reply == uint8_t(REPLY_NOACT) ) {
    ROS_INFO_STREAM( "Starting Success with code 0x" << std::hex << std::uppercase << int(reply) );
  } else {
    ROS_INFO_STREAM( "Start failed with code 0x" << std::hex << std::uppercase << int(reply) );
  }

  // ping
  ROS_INFO_STREAM( "Sending PING  command ... " );
  Serial.write( COMMAND_PING );
  reply = Serial.read();
  if( reply == uint8_t(REPLY_PING) ) {
    ROS_INFO_STREAM( "Ping Success with code 0x" << std::hex << std::uppercase << int(reply) );
  } else {
    ROS_INFO_STREAM( "Ping failed with code 0x" << std::hex << std::uppercase << int(reply) );
  }

  // flush any residues
  Serial.flush();

  // reset odom related information
  odom_pose_x   = 0.00f;
  odom_pose_y   = 0.00f;
  odom_orie_yaw = 0.00f;

  ticks_curr      = { 0, 0 };
  ticks_prev      = { 0 ,0 };
  enc_stamp_prev  = ros::Time::now();

  return 1;
}

void cb_cmdVel( const geometry_msgs::Twist::ConstPtr& msg_cmdVel ) {
  ROS_INFO_STREAM( "[cmd_vel]: v_x = " << msg_cmdVel->linear.x
                  << "  v_y = " << msg_cmdVel->linear.y
                  << "  v_z = " << msg_cmdVel->linear.z
                  << "  w_x = " << msg_cmdVel->angular.x
                  << "  w_y = " << msg_cmdVel->angular.y
                  << "  w_z = " << msg_cmdVel->angular.z );

  motor_command_t mtrCmd;
  float leftRpm   = 0.00f;
  float rightRpm  = 0.00f;

  // calculate RPMs for both wheels
  leftRpm   = (msg_cmdVel->linear.x - msg_cmdVel->angular.z*WHEEL_TRACK) * 60 / (2 * CONST_PI * WHEEL_RADIUS );
  rightRpm  = (msg_cmdVel->linear.x + msg_cmdVel->angular.z*WHEEL_TRACK) * 60 / (2 * CONST_PI * WHEEL_RADIUS );

  mtrCmd.u8_motorDirection  = (((leftRpm>=0)?COMMAND_DIR_FWD:COMMAND_DIR_REV)<<4) +
                                ((rightRpm>=0)?COMMAND_DIR_FWD:COMMAND_DIR_REV);

  // take absolute values
  leftRpm   = (leftRpm>0.00)?leftRpm:-leftRpm;
  rightRpm  = (rightRpm>0.00)?rightRpm:-rightRpm;
  // constrain them to legal values ( < 5kmph)
  leftRpm   = (leftRpm<96)?leftRpm:96.00;
  rightRpm  = (rightRpm<96)?rightRpm:96.00;
  // set correction for RPM [25,229]
  leftRpm   += 25.00;
  rightRpm  += 25.00;

  // set motor PWM values in data packet
  mtrCmd.u8_pwmLeft   = (uint8_t) leftRpm;
  mtrCmd.u8_pwmRight  = (uint8_t) rightRpm;

  // write this to base_link
  Serial.write( COMMAND_SET_MOTOR );
  Serial.write( (uint8_t*)&mtrCmd, sizeof(mtrCmd) );

  // ROS_INFO_STREAM( "RPM: " << leftRpm << ", " << rightRpm );
  // ROS_INFO_STREAM( "Motor Commad: 0x"
  //                     << std::hex << std::uppercase << int(mtrCmd.u8_motorDirection) << "    "
  //                     << std::hex << std::uppercase << int(mtrCmd.u8_pwmLeft) << "    "
  //                     << std::hex << std::uppercase << int(mtrCmd.u8_pwmRight) );
  uint8_t reply = Serial.read();
  if( reply == uint8_t(REPLY_SUCCESS) ) {
    // ROS_INFO_STREAM( "SUCCESS : Reponse Code 0x" << std::hex << std::uppercase << int(reply) );
  } else {
    ROS_INFO_STREAM( "FAILED: Reponse Code 0x" << std::hex << std::uppercase << int(reply) );
  }
}
