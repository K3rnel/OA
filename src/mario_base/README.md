# Mario Base
Package to interafce with the robot base.

### Setting Up
 1. Clone this repository
 2. use `git submodule init` to initialize dependencies
 3. use `git submodule update` to pull dependencies (for more info on submodules, https://chrisjean.com/git-submodules-adding-using-removing-and-updating/)

### Debug
Build module with `catkin build --cmake-args -DCMAKE_BUILD_TYPE=Debug`

### TODO
1. Fix covaraince calculations
2. Upload arduino firmware from command line
