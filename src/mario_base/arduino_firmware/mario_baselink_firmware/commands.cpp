/*
Handlers for different commands
*/
#include "motor.h"
#include "commands.h"

extern bool b_flagStart;

extern uint32_t ledLastUpdate;
extern uint32_t ledUpdatePeriod;

extern encoder_data_t encData;
extern uint32_t encoderLastUpdate;

uint8_t onCmd_start() {
  if( b_flagStart ) {
    Serial.write( REPLY_NOACT );
  } else {
    b_flagStart = true;
    digitalWrite( LED_PIN, LOW );
    ledUpdatePeriod = 750;  // in ms
    ledLastUpdate   = millis();
    Serial.write( REPLY_SUCCESS );
  }
}

uint8_t onCmd_stop() {
  if( b_flagStart ) {
    b_flagStart = false;
    ledUpdatePeriod = 250;  // in ms
    Serial.write( REPLY_SUCCESS );
  } else {
    Serial.write( REPLY_NOACT );
  }
}

uint8_t onCmd_reset() {
  // do reset
  encData = { 0, 0 };
  digitalWrite( LED_PIN, HIGH );  delay( 100 );
  digitalWrite( LED_PIN, LOW );   delay( 100 );
  digitalWrite( LED_PIN, HIGH );  delay( 100 );
  digitalWrite( LED_PIN, LOW );   delay( 100 );
  ledLastUpdate = millis();

  initMotor();
  Serial.write( REPLY_SUCCESS );
}

uint8_t onCmd_ping() {
  Serial.write( REPLY_PING );
}

uint8_t onCmd_unknown( byte _byte ) {
  Serial.write( REPLY_ERROR );
  Serial.write( _byte );
}

uint8_t onCmd_sendEncData() {
  // read encoder data of master due
#if defined (__arm__) && defined (__SAM3X8E__) // Arduino Due compatible
  encData.i32_ticksLeft = -(int32_t) REG_TC0_CV0;
#else // for testing in other arduinos(Nano)
  encData.i32_ticksLeft = 0;
#endif
  // encData.i32_ticksRight will be filled by SerialEvent
  Serial.write( (uint8_t*)&encData, sizeof(encData) );
  Serial.flush();
}

uint8_t onCmd_setMotor( ) {
  // read motor data
  motor_command_t motorCmd;
  uint8_t bytesRead = Serial.readBytes( (uint8_t*)&motorCmd, sizeof(motorCmd) );
  if( bytesRead == sizeof(motorCmd) ) {
    setMotorSpeed( 
      ((motorCmd.u8_motorDirection>>4)==COMMAND_DIR_FWD)?DIR_LEFT_FWD:!DIR_LEFT_FWD,
      motorCmd.u8_pwmLeft,
      ((motorCmd.u8_motorDirection&0x0F)==COMMAND_DIR_FWD)?DIR_RIGHT_FWD:!DIR_RIGHT_FWD,
      motorCmd.u8_pwmRight );
      Serial.write( REPLY_SUCCESS );
  } else {
    Serial.write( REPLY_NOACT );
  }
  
}


