// Motor control functions

#include "motor.h"

uint8_t initMotor() {
  // set pin modes
  pinMode( MOTOR_PWM_PIN_LEFT, OUTPUT );
  pinMode( MOTOR_EN_PIN_LEFT, OUTPUT );
  pinMode( MOTOR_DIR_PIN_LEFT, OUTPUT );
  pinMode( MOTOR_PWM_PIN_RIGHT, OUTPUT );
  pinMode( MOTOR_EN_PIN_RIGHT, OUTPUT );
  pinMode( MOTOR_DIR_PIN_RIGHT, OUTPUT );

  // set initial conditions
  digitalWrite( MOTOR_EN_PIN_LEFT, HIGH ); // allow free wheeling?
  digitalWrite( MOTOR_DIR_PIN_LEFT, DIR_LEFT_FWD );
  analogWrite( MOTOR_PWM_PIN_LEFT, 25 ); // ??
  digitalWrite( MOTOR_EN_PIN_RIGHT, HIGH );  // allow free wheeling?
  digitalWrite( MOTOR_DIR_PIN_RIGHT, DIR_RIGHT_FWD );
  analogWrite( MOTOR_PWM_PIN_RIGHT, 25 ); // ?? 
  
}

uint8_t setMotorSpeed( bool leftDir, uint8_t leftPwm, bool rightDir, uint8_t rightPwm ) {
  digitalWrite( MOTOR_DIR_PIN_LEFT, leftDir );
  analogWrite( MOTOR_PWM_PIN_LEFT, leftPwm );
  digitalWrite( MOTOR_DIR_PIN_RIGHT, rightDir );
  analogWrite( MOTOR_PWM_PIN_RIGHT, rightPwm );
  
}

