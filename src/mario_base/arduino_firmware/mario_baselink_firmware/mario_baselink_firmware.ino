// motor controll for baselink
// Inputs: raw pwm for left and right wheel
// OUtputs: raw ticks from each wheel as uint32_t

#include "motor.h"
#include "commands.h"


bool b_flagStart = false;

uint32_t ledLastUpdate        = 0;
uint32_t ledUpdatePeriod      = 250;  // update period in milliseconds

// #if defined (__arm__) && defined (__SAM3X8E__) // Arduino Due compatible
// Encoder configuration
const int quad_A = 2;
const int quad_B = 13;
const unsigned int mask_quad_A = digitalPinToBitMask(quad_A);
const unsigned int mask_quad_B = digitalPinToBitMask(quad_B); 
// #endif

// Encoder data
encoder_data_t encData = { 0, 0 };
uint32_t encoderLastUpdate    = 0;
uint32_t encoderUpdatePeriod  = 100;  // update period in milliseconds


void setup() {
  Serial.begin( 115200 );
// #if defined (__arm__) && defined (__SAM3X8E__) // Arduino Due compatible
  Serial3.begin( 115200 );  // for slave QEI
// #endif

  // Enable visual feed back
  pinMode( LED_PIN, OUTPUT );
  digitalWrite( LED_PIN, HIGH );
  ledLastUpdate = millis();

// #if defined (__arm__) && defined (__SAM3X8E__) // Arduino Due compatible
  // ---------Enabling Registers for QEI-----------------
  // activate peripheral functions for quad pins
  REG_PIOB_PDR = mask_quad_A;     // activate peripheral function (disables all PIO functionality)
  REG_PIOB_ABSR |= mask_quad_A;   // choose peripheral option B
  REG_PIOB_PDR = mask_quad_B;     // activate peripheral function (disables all PIO functionality)
  REG_PIOB_ABSR |= mask_quad_B;   // choose peripheral option B
  REG_PMC_PCER0 = (1 << 27);      // activate clock for TC0
  REG_TC0_CMR0 = 5;               // select XC0 as clock source and set capture mode
  // activate quadrature encoder and position measure mode, no filters
  REG_TC0_BMR = (1 << 9) | (1 << 8) | (0 << 12); //we are detecting only half resolution
  // enable the clock (CLKEN=1) and reset the counter (SWTRG=1)
  // SWTRG = 1 necessary to start the clock!!
  REG_TC0_CCR0 = 5;
// #endif

  // intialize motors
  initMotor();
}

void loop() {
  if( !b_flagStart &&
      (millis()-ledLastUpdate > ledUpdatePeriod) ) {
    digitalWrite( LED_PIN, !digitalRead(LED_PIN) );
    ledLastUpdate = millis();
  } else {
    if( millis() - ledLastUpdate > ledUpdatePeriod ) {
      digitalWrite( LED_PIN, !digitalRead(LED_PIN) );
      ledLastUpdate = millis();
    }
  }

  // Fake encoder ticks data for testing
  /*
  if( b_flagStart &&  // if started
      (millis()-encoderLastUpdate > encoderUpdatePeriod) ) { // and last encoder data was sent XYZms ago
    encData.i32_ticksLeft   += 20;
    encData.i32_ticksRight  += 5;
    encoderLastUpdate = millis();
  }
  */
}

void serialEvent() {
  
  if( Serial.available() > 0 ) {  // if serial data from computer
    byte _byte = Serial.read();
    if( _byte == COMMAND_START ) {
      onCmd_start();
    } else if( _byte == COMMAND_STOP ) {
      onCmd_stop();
    } else if( _byte == COMMAND_RESET ) {
      onCmd_reset();
    } else if( _byte == COMMAND_PING ) {
      onCmd_ping();
    } else if( _byte == COMMAND_SEND_ENC_DATA ) {
      onCmd_sendEncData();
    } else if( _byte == COMMAND_SET_MOTOR ) {
      onCmd_setMotor();
    } else {
      onCmd_unknown( _byte );
    }
    Serial.flush();
  }
}

// #if defined (__arm__) && defined (__SAM3X8E__) // Arduino Due compatible
void serialEvent3() {
  if( Serial3.available() > 0 ) { // QEI data from slave Due
    String str_ticksRight   = Serial3.readStringUntil( 's' );
    encData.i32_ticksRight  = atoi( str_ticksRight.c_str() );
  }
}
// #endif

