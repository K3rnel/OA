// Contains definitions for all hardware connections

#include "Arduino.h"
#include "system.h"

#define DIR_LEFT_FWD  HIGH
#define DIR_RIGHT_FWD LOW

typedef struct {
  uint8_t u8_motorDirection;  // upper 4 bits for Left Motor, lower nibble for right motor
  uint8_t u8_pwmLeft;
  uint8_t u8_pwmRight;
}__attribute__((__packed__))motor_command_t;

uint8_t initMotor();

uint8_t setMotorSpeed( bool leftDir, uint8_t leftPwm, bool rightDir, uint8_t rightPwm );
