// Hardware configuration

#define LED_PIN 13

#define MOTOR_PWM_PIN_LEFT  9
#define MOTOR_EN_PIN_LEFT  23
#define MOTOR_DIR_PIN_LEFT 25

#define MOTOR_PWM_PIN_RIGHT  8
#define MOTOR_EN_PIN_RIGHT  22
#define MOTOR_DIR_PIN_RIGHT 24
