#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
// Includes ROS action - high level interface of navigation stack. 
#include <actionlib/client/simple_action_client.h>
#include <goals/conversions.h>
#include <sensor_msgs/NavSatFix.h>

using namespace gps_common;

// ros::Publisher goal_pub;

double lat, lon;

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

void gps_cb(const sensor_msgs::NavSatFixConstPtr& fix)
{

}



int main(int argc, char** argv){

	ros::init(argc,argv,"simple_navigation_goals");

	ros::NodeHandle nh;
	ros::Subscriber Fix = nh.subscribe("/navsat/fix",10,gps_cb);
	// goal_pub = nh.advertise<move_base_msgs::MoveBaseGoal>("/goal", 10);

	if(!(argc >2))
	{
		ROS_INFO("Plese specify Goal Coordinates");
		return 0;
	}

	sscanf(argv[1],"%lf",&lat);
	sscanf(argv[2],"%lf",&lon);

	double x_current,y_current,x_final,y_final,delx,dely;
	std::string zone_current,zone_final;

	

	// UTM(12.9925386153,80.2325530658,&y_current,&x_current);
	UTM(12.9916434565,80.233749335,&y_current,&x_current);
	UTM(lat,lon,&y_final,&x_final);


	delx = (x_final - x_current);
	dely = -(y_final - y_current);

	ROS_INFO("x_current:%lf x_final:%lf \t y_current:%lf y_final:%lf \t delx:%lf dely:%lf",x_current,x_final,y_current,y_final,delx,dely);
	
	MoveBaseClient ac("move_base",true);
	
	while(!ac.waitForServer(ros::Duration(0.5))){
		ROS_INFO("Waiting for the move_base action server to come up");
	}

	move_base_msgs::MoveBaseGoal goal;
	
	goal.target_pose.header.frame_id = "base_link";
	goal.target_pose.header.stamp = ros::Time::now();
	goal.target_pose.pose.position.x = delx;
	goal.target_pose.pose.position.y = dely;
	goal.target_pose.pose.orientation.w = 1;
	ROS_INFO("Sending goal");

	ac.sendGoal(goal);
	ac.waitForResult();
	if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
		ROS_INFO("Hooray, the base moved 1 meter forward");
	else
		ROS_INFO("The base failed to move forwared 1 meter for some reason");

	return 0;	
	// ros::spin();
}