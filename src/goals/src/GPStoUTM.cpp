#include <ros/ros.h>
#include <geometry_msgs/Point.h>
#include <sensor_msgs/NavSatFix.h>
#include <goals/conversions.h>
using namespace std;
ros::Publisher point_pub;
bool firstflag = true;
double initx =0;
double inity =0;
void gps_cb(const sensor_msgs::NavSatFix::ConstPtr & msg){
   double x= 0, y= 0 ;
   cout.precision(17);
   char * zone = new char;

   gps_common::LLtoUTM(msg->latitude,msg->longitude,y,x,zone);
   geometry_msgs::Point out; 
   if(firstflag){
   	initx = x;
   	inity = y;
   	firstflag = false;
   }
   out.x = x- initx; 
   out.y = y - inity ; 
   point_pub.publish(out);
   // cout << msg->latitude << "  " << msg->longitude << endl << x << " " << y << endl;
}

int main(int argc, char ** argv) {
   ros::init(argc, argv, "modif_odom");

   ros::NodeHandle nh;

   ros::Subscriber s = nh.subscribe("/navsat/fix", 2, &gps_cb);

   point_pub = nh.advertise<geometry_msgs::Point>("odom/gps", 2);
   ros::spin();
}