// #include <ros/ros.h>
// #include <move_base_msgs/MoveBaseAction.h>
// // Includes ROS action - high level interface of navigation stack. 
// #include <actionlib/client/simple_action_client.h>

// typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;
// int main(int argc, char** argv){

// 	ros::init(argc,argv,"simple_navigation_goals");

// 	//tell the action client that we want to spin a thread by default 

// 	MoveBaseClient ac("move_base",true);
// // This line constructs an action client that we'll se to communicate with action named "move_base" that adheres to the MoveBaseAction interface. 
// // This also tells the action client to start a thread to call ros::spin() so that ROS callbacks will be processed by passing true as the second argument of the MoveBaseClient contructor. 
	
// 	//wait for the action server to come up
// 	while(!ac.waitForServer(ros::Duration(0.5))){
// 		ROS_INFO("Waiting for the move_base action server to come up");
// 	}

// 	move_base_msgs::MoveBaseGoal goal;

// 	//we'll send a goal to the robot to move 1 meter forward 
// 	goal.target_pose.header.frame_id = "base_link";
// 	goal.target_pose.header.stamp = ros::Time::now();

// 	goal.target_pose.pose.position.x = 1.0;
// 	goal.target_pose.pose.position.y = 1.0;

// 	ROS_INFO("Sending goal");

// 	ac.sendGoal(goal);

// 	ac.waitForResult();

// 	if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
// 		ROS_INFO("Hooray, the base moved 1 meter forward");
// 	else
// 		ROS_INFO("The base failed to move forwared 1 meter for some reason");

// 	return 0;
// }



#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

int main(int argc, char** argv){

double x, y;
if(!(argc >2))
  {
    ROS_INFO("specify coordinates");  
  }
  
  sscanf(argv[1],"%lf",&x);
  sscanf(argv[2],"%lf",&y);


  ros::init(argc, argv, "simple_navigation_goals");

  //tell the action client that we want to spin a thread by default
  MoveBaseClient ac("move_base", true);

  //wait for the action server to come up
  while(!ac.waitForServer(ros::Duration(5.0))){
    ROS_INFO("Waiting for the move_base action server to come up");
  }

  move_base_msgs::MoveBaseGoal goal;

  //we'll send a goal to the robot to move 1 meter forward
  goal.target_pose.header.frame_id = "map";
  goal.target_pose.header.stamp = ros::Time::now();

  goal.target_pose.pose.position.x = x;
  goal.target_pose.pose.position.x = y;
  goal.target_pose.pose.orientation.w = 1.0;

  ROS_INFO("Sending goal");
  ac.sendGoal(goal);

  ac.waitForResult();

  if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    ROS_INFO("Hooray, the base moved 1 meter forward");
  else
    ROS_INFO("The base failed to move forward 1 meter for some reason");

  return 0;
}	