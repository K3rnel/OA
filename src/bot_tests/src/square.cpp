#include "ros/ros.h"
#include "geometry_msgs/Twist.h"
int main(int argc, char  **argv)
{
	ros ::init(argc,argv,"squareNode") ;
	ros :: NodeHandle A;

	double length,speed;

	speed = atof(argv[1]) ;
	length = atof(argv[2]) ;

	ros::Publisher velocity = A.advertise<geometry_msgs::Twist>("/cmd_vel",1000) ;
	ros::Rate loop_rate(10) ;
	while(ros::ok())
	{
		ros::Time turn = ros::Time::now() ;
		while(ros::Time::now() - turn < ros::Duration(length/speed))
		{
			geometry_msgs::Twist m ;
			m.linear.x =  speed ;
			m.angular.z = 0 ;
			velocity.publish(m);
			ros::spinOnce() ;
			loop_rate.sleep() ;
		}

		turn = ros::Time::now();

		while(ros::Time::now() - turn < ros::Duration(1.0))
		{
			;
		}

		turn = ros::Time::now() ;
		while(ros::Time::now() - turn < ros::Duration(1.0))
		{
			geometry_msgs::Twist m ;
			m.linear.x =  0 ;
			m.angular.z = 1.57 ;
			velocity.publish(m);
			ros::spinOnce() ;
			loop_rate.sleep() ;
		}
	}


	return 0;
}