#include "ros/ros.h"
#include "geometry_msgs/Twist.h"
#include <iostream> 
#include <stdlib.h>

using namespace std;
using namespace ros;

int main(int argc, char** argv)
{
	init(argc, argv, "circleNode");

	NodeHandle nh;

	Publisher circlePub = nh.advertise<geometry_msgs::Twist>("cmd_vel",1000);

	Rate publishRate(10);

	float vel = (float)atof(argv[1]);
	float radius = (float)atof(argv[2]);

	while(ok())
	{
		geometry_msgs::Twist msg;
		msg.linear.x = vel;
		msg.angular.z = vel/radius;

		circlePub.publish(msg);
		
		publishRate.sleep();
	}

	return 0;
}