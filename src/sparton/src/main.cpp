#include<iostream>
using namespace std;
#include "IMU_AHRS8.h"

int main(int argc, char** argv)
{
	ros::init(argc,argv,"Sparton_imu");
	AHRS8 IMU;
	//ros::Rate loop_rate(10);

	if(IMU.serialInitialize("/dev/ttyUSB0"))
	{
		cout<<"Port Open Successful"<<endl;
	}
	else
	{
		cout<<"Please check the connect"<<endl;
	}

	IMU.fetchSerialData();

	
	return 0;
}
