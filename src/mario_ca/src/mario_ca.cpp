// Mario Collision Avoidance

#include <iostream>

#include "ros/ros.h"
#include "std_msgs/Bool.h"
#include "geometry_msgs/Twist.h"
#include "sensor_msgs/LaserScan.h"
#include <queue>

std::queue<geometry_msgs::Twist> pastVel;
bool retracePath = false;

void teleop_vel_cb( const geometry_msgs::Twist::ConstPtr& msg ) {
  pastVel.push( *msg );
  if( pastVel.size() > 20 ) {
    pastVel.pop();
  }
}

void scan_cb( const sensor_msgs::LaserScan::ConstPtr& msg ) {
  int noOfSamples = (msg->angle_max-msg->angle_min)/msg->angle_increment+1;

  // 1.4 rad ~ 80 deg
  float theta_min = -1.2, theta_max = +1.2;
  unsigned minPoints  = 30;
  float minDistance   = 0.60;
  //calculate indices
  int minIdx = (theta_min-msg->angle_min)/msg->angle_increment;
  int maxIdx = (theta_max-msg->angle_min)/msg->angle_increment;

  unsigned count = 0;
  for( unsigned i = minIdx; i <= maxIdx; i++ ) {
    if( msg->ranges[i] < minDistance ) {
      count++;
    }
  }
  if( count > minPoints ) {
    // we are near an obstacle - perform retrace
    retracePath = true;
  }

  ROS_INFO_STREAM( "  [/scan] " << msg->header.seq << "  "
                      // << " :: ranges[" << sizeof(msg->ranges) << "]  "
                      << noOfSamples << " samples    "
                      << count << " points"  );

}

int main( int argc, char *argv[] ) {

  //Initialize not
  ros::init( argc, argv, "mario_ca" );

  ros::NodeHandle nh;

  // Publish locks
  ros::Publisher pub_cmdVel = nh.advertise<geometry_msgs::Twist>( "cmd_vel_final", 30 );

  // subscribe to cmd_vel
  ros::Subscriber sub_cmdVel = nh.subscribe( "cmd_vel_teleop", 30, teleop_vel_cb );
  // subscribe to laser scan
  ros::Subscriber sub_scan   = nh.subscribe( "scan", 30, scan_cb );

  ros::Rate loop_rate( 10 );
  ros::Rate retraceRate( 10 );

  bool state = true;

  while( ros::ok() ) {

    if( retracePath ) {
      retracePath = false;
      ROS_INFO_STREAM( "Playing back ... " );
      while( !pastVel.empty() ) {
          geometry_msgs::Twist thisVel;
          geometry_msgs::Twist manVel = pastVel.front();  pastVel.pop();
          thisVel.linear.x  = -manVel.linear.x;
          thisVel.linear.y  = -manVel.linear.y;
          thisVel.linear.z  = -manVel.linear.z;
          thisVel.angular.x = -manVel.angular.x;
          thisVel.angular.y = -manVel.angular.y;
          thisVel.angular.z = -manVel.angular.z;

          pub_cmdVel.publish( thisVel );
          retraceRate.sleep();
      }
      ROS_INFO_STREAM( "End of playback" );
    } else {
      if( !pastVel.empty() ) {
        pub_cmdVel.publish( pastVel.back() );
      }
    }

    ros::spinOnce();
    loop_rate.sleep();
  }

  return 0;
}
