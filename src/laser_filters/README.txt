1. Compile the source code 
2. roslaunch abhiyan_nav roswithimu.launch 
3. roslaunch laser_filters mylaserfilter.launch 
4. rosrun rviz rviz
5. In LaserScan section select the topic /scan_filtered instead of /scan
6. Compare the difference between /scan topic and /scan_filtered
7. /scan_filtered should give values from -180 to 180 degrees.