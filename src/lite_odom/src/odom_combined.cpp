/*
 * dagny_odom.cpp
 *
 * Take simple odometry messages and republish them as full Odometry messages
 *  and as a tf transform
 *
 * Author: Austin Hendrix
 */


#include <iostream>
#include <string>
#include <std_msgs/String.h>
#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <litemsgs/lite.h>
#include <tf/transform_broadcaster.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <cmath>
 using namespace std;
ros::Publisher odom_pub,odom_pub2;


nav_msgs::Odometry twist1;

void odom_cb( const nav_msgs::Odometry::ConstPtr & msg ) {
   twist1.twist.twist=msg->twist.twist;
}

void odom_combined(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg ) {
   
   static tf::TransformBroadcaster odom_tf;//Removed it according to 
   // odometry message
   nav_msgs::Odometry out;

   out.header = msg->header;
   out.child_frame_id = "base_link";   
   out.pose.pose = msg->pose.pose;
   out.twist.twist = twist1.twist.twist;
   for (int i=0;i<36;i++)
   {
          if(i==0 || (i>5 && i%7 == 0))
      out.twist.covariance[i]=0.01;
      else
      out.twist.covariance[i]=0.00;
 
   }
      odom_pub.publish(out);
   // tf transform
   // geometry_msgs::TransformStamped transform;
   // transform.header = msg->header;
   // transform.child_frame_id = "base_link";
   // transform.transform.translation.x = msg->pose.pose.position.x;
   // transform.transform.translation.y = msg->pose.pose.position.y;
   // transform.transform.translation.z = msg->pose.pose.position.z;
   // transform.transform.rotation.x = 0;
   // transform.transform.rotation.y = 0;
   // transform.transform.rotation.z = msg->pose.pose.orientation.z;
   // transform.transform.rotation.w = msg->pose.pose.orientation.w;
   // odom_tf.sendTransform(transform);

}


int main(int argc, char ** argv) {
   ros::init(argc, argv, "modif_odom");

   ros::NodeHandle nh;

   ros::Subscriber s = nh.subscribe("odom_lite", 2, &odom_cb);
   ros::Subscriber t = nh.subscribe("robot_pose_ekf/odom_combined",2, &odom_combined);
   ROS_INFO("Odom!");
   //odom_pub = nh.advertise<nav_msgs::Odometry>("odom_combined", 2);
   
   odom_pub = nh.advertise<nav_msgs::Odometry>("odom", 2);
   ros::spin();
}
