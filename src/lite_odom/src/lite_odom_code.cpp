/*
 * dagny_odom.cpp
 *
 * Take simple odometry messages and republish them as full Odometry messages
 *  and as a tf transform
 *
 * Author: Austin Hendrix
 */

#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <litemsgs/lite.h>
#include <tf/transform_broadcaster.h>
#include <sensor_msgs/Imu.h>
#include <tf/tf.h>
ros::Publisher odom_pub;

geometry_msgs::Quaternion orient;
geometry_msgs::Vector3 angular_velocity;

double dx, dy, velX, velY, pose_x, pose_y;
double dT = 0.015;
void imu_cb( const sensor_msgs::Imu::ConstPtr & msg)
{
	orient = msg->orientation;
   angular_velocity = msg->angular_velocity;
}

void odom_cb( const litemsgs::lite::ConstPtr & msg ) {
   //static tf::TransformBroadcaster odom_tf;//Removed it according to 
   // odometry message
   nav_msgs::Odometry out;
   // Distance message from Arduino
   double Distance = msg->pose.position.x;
   // Converting from geometry_msgs/Quaternion to tf/Quaternion

   tf::Quaternion q(orient.x, orient.y, orient.z, orient.w);
   
   // q.pose.pose.orientation.x = orient.x;
   // q.pose.pose.orientation.y = orient.y;
   // q.pose.pose.orientation.z = orient.z;
   // q.pose.pose.orientation.x = orient.w;
   // Converting quaternion from IMU to Euler Angles using tf
   // ROS_INFO("Distance : %f",&Distance);

   tf::Matrix3x3 m(q);
   double roll, yaw, pitch; 
   m.getRPY(roll,pitch,yaw);
   //Calculating pose and twist
   dy = Distance*sin(yaw);
   dx = Distance*cos(yaw);
   velX = dx/dT;
   velY = dy/dT;
   pose_x += dx;
   pose_y += dy;

   //Creating pose part of nav_msgs/Odometry  
   out.pose.pose.position.x = pose_x;
   out.pose.pose.position.y = pose_y;
   out.pose.pose.position.z = 0;
   out.pose.pose.orientation = orient;

   for (int i=0;i<36;i++)
   {
		if(i==0 || (i>5 && i%7 == 0))
		out.pose.covariance[i]=0.01;
		else
		out.pose.covariance[i]=0.00;
   }

   //Creating twist part of nav_msgs/Odometry 
   out.twist.twist.linear.x = velX;
   out.twist.twist.linear.y = velY;
   out.twist.twist.linear.z = 0;
   out.twist.twist.angular = angular_velocity;
   for (int i=0;i<36;i++)
   {
          if(i==0 || (i>5 && i%7 == 0))
		out.twist.covariance[i]=0.01;
		else
		out.twist.covariance[i]=0.00;
   }

   out.header = msg->header;
   out.child_frame_id = msg->child_frame_id;
   
   odom_pub.publish(out);

}

int main(int argc, char ** argv) {
   ros::init(argc, argv, "modif_odom");

   ros::NodeHandle nh;

   ros::Subscriber s = nh.subscribe("odom_sub", 2, &odom_cb);
   ros::Subscriber e = nh.subscribe("imu", 2, &imu_cb);

   ROS_INFO("Odom!");
   //odom_pub = nh.advertise<nav_msgs::Odometry>("odom_combined", 2);
   // odom_pub = nh.advertise<nav_msgs::Odometry>("odom", 2);
   odom_pub = nh.advertise<nav_msgs::Odometry>("odom_lite", 2);
   ros::spin();
}