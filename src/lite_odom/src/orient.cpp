/*
 * dagny_odom.cpp
 *
 * Take simple odometry messages and republish them as full Odometry messages
 *  and as a tf transform
 *
 * Author: Austin Hendrix
 */

#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <litemsgs/lite.h>
#include <tf/transform_broadcaster.h>
#include <geometry_msgs/Vector3.h>
 #include <geometry_msgs/Pose2D.h>
ros::Publisher odom_pub;

// geometry_msgs::Twist twist1;
// twist1.linear.x=0;
// twist1.linear.y=0;
// twist1.linear.z=0;
// twist1.angular.x=0;
// twist1.angular.y=0;
// twist1.angular.z=0;

// nav_msgs::Odometry twist1;

void odom_cb( const nav_msgs::Odometry::ConstPtr & msg ) {
   // Geometry msg type Quaternion
   geometry_msgs::Pose2D out;
   // geometry_msgs::Quaternion q;
   double roll, pitch, yaw;

 tf::Quaternion q(msg->pose.pose.orientation.x, msg->pose.pose.orientation.y, msg->pose.pose.orientation.z, msg->pose.pose.orientation.w);
   // q = msg->->pose.pose.orientation;
   
   tf::Matrix3x3 m(q);
   m.getRPY(roll, pitch, yaw);
   float pi = 3.14;
   out.x = msg->pose.pose.position.x;
   out.y = msg->pose.pose.position.y;

   // out.orientation.x = roll*180/pi;
   // out.orientation.y = pitch*180/pi;
   // out.orientation.z = yaw*180/pi;
   // out.orientation.w = 0;

   out.theta = yaw*180/pi;

   odom_pub.publish(out);

}



int main(int argc, char ** argv) {
   ros::init(argc, argv, "modif_odom");

   ros::NodeHandle nh;

   ros::Subscriber s = nh.subscribe("odom", 2, &odom_cb);
   // ros::Subscriber t = nh.subscribe("robot_pose_ekf/odom_combined",2, &odom_combined);
   ROS_INFO("Odom!");
   //odom_pub = nh.advertise<nav_msgs::Odometry>("odom_combined", 2);
   // odom_pub = nh.advertise<nav_msgs::Odometry>("odom", 2);
   odom_pub = nh.advertise<geometry_msgs::Pose2D>("orient", 2);
   // odom_pub = nh.advertise<nav_msgs::Odometry>("odom", 2);
   ros::spin();
}